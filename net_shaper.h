/* SPDX-License-Identifier: GPL-2.0-or-later */

#ifndef _NET_SHAPER_H_
#define _NET_SHAPER_H_

/**
 * enum shaper_metric - the metric of the the shaper
 * @SHAPER_METRIC_PPS: Shaper operates on a packets per second basis
 * @SHAPER_METRIC_BPS: Shaper operates on a bits per second basis
 */
enum shaper_metric {
	SHAPER_METRIC_PPS;
	SHAPER_METRIC_BPS;
};

/**
 * struct shaper_info - represents a shaping node on the NIC H/W
 * @metric: Specify if the bw limits refers to PPS or BPS
 * @bw_min: Minimum guaranteed rate for this shaper
 * @bw_max: Maximum peak bw allowed for this shaper
 * @burst: Maximum burst for the peek rate of this shaper
 * @priority: Scheduling priority for this shaper
 * @weight: Scheduling weight for this shaper
 */
struct shaper_info {
	enum shaper_metric metric;
	u64 bw_min;	/* minimum guaranteed bandwidth, according to metric */
	u64 bw_max;	/* maximum allowed bandwidth */
	U32 burst;	/* maximum burst in bytes for bw_max */
	u32 priority;	/* scheduling strict priorty */
	u32 weight;	/* scheduling WRR weight*/
};

/**
 * enum shaper_scope - the different scopes where a shaper could be attached
 * @SHAPER_SCOPE_PORT:   The root shaper for the whole H/W.
 * @SHAPER_SCOPE_NETDEV: The main shaper for the given network device.
 * @SHAPER_SCOPE_VF:     The shaper is attached to the given virtual function.
 * @SHAPER_SCOPE_QUEUE_GROUP: The shaper groups multiple queues under the given
 * 			 device of VF
 * @SHAPER_SCOPE_QUEUE:  The shaper is attached to the given queue, belonging
 * 			 to the given device or VF
 *
 * SHAPER_SCOPE_PORT and SHAPER_SCOPE_VF are only available on
 * PF devices, usually inside the host/hypervisor.
 * SHAPER_SCOPE_NETDEV, SHAPER_SCOPE_QUEUE_GROUP and
 * SHAPER_SCOPE_QUEUE are available on both PFs and VFs devices.
 */
enum shaper_scope {
	SHAPER_SCOPE_PORT,
	SHAPER_SCOPE_NETDEV,
	SHAPER_SCOPE_VF,
	SHAPER_SCOPE_QUEUE_GROUP,
	SHAPER_SCOPE_QUEUE,
};

/**
 * struct shaper_ops - Operations on shaper hierarchy
 * @add: Creates a new shaper in the specified scope.
 * @delete: Delete the specified shaper.
 * @set: Modify the existing shaper.
 *
 * The netdevice exposes a pointer to these ops.
 *
 * The initial shaping configuration ad device initialization is empty/
 * a no-op/does not constraint the b/w in any way.
 * The network core keeps track of the applied user-configuration in
 * per device storage.
 *
 * Each shaper is uniquely identified within the device with an 'handle',
 * dependent on the shaper scope and other data, see @shaper_make_handle()
 */
struct shaper_ops {
	/* add - Add a shaper inside the shaper hierarchy
	 * @dev: netdevice to operate on
	 * @handle: the shaper indetifier
	 * @shaper: configuration of shaper
	 * @extack: Netlink extended ACK for reporting errors.
	 *
	 * Return:
	 * * %0 on success
	 * * %-EOPNOTSUPP - Operation is not supported by hardware, driver, or core
	 *                  for any reason. @extack should be set to text describing
	 *                  the reason.
	 * * Other negative error values on failure.
	 *
	 * Examples or reasons this operation may fail include:
	 * * H/W resources limits.
	 * * Can’t respect the requested bw limits.
	 */
	int (*add)(struct net_device *dev, u32 handle,
		   const struct shaper_info *shaper, struct netlink_ext_ack *extack);

	/* set - Update the specified shaper, if it exists
	 * @dev: Netdevice to operate on.
	 * @handle: the shaper identifier
	 * @shaper: Configuration of shaper.
	 * @extack: Netlink extended ACK for reporting errors.
	 *
	 * Return:
	 * * %0 - Success
	 * * %-EOPNOTSUPP - Operation is not supported by hardware, driver, or core
	 *                  for any reason. @extack should be set to text describing
	 *                  the reason.
	 * * Other negative error values on failure.
	 */
	int (*set)(struct net_device *dev, u32 handle,
		   const struct shaper_info *shaper, struct netlink_ext_ack *extack);

	/* delete - Removes a shaper from the NIC
	 * @dev: netdevice to operate on.
	 * @handle: the shaper identifier
	 * @extack: Netlink extended ACK for reporting errors.
	 *
	 * Return:
	 * * %0 - Success
	 * * %-EOPNOTSUPP - Operation is not supported by hardware, driver, or core
	 *                  for any reason. @extack should be set to text describing
	 *                  the reason.
	 * * Other negative error value on failure.
	 */
	int (*delete)(struct net_device *dev, u32 handle,
		      struct netlink_ext_ack *extack);

	/* Move - change the parent id of the specified shaper
	 * @dev: netdevice to operate on.
	 * @handle: unique identifier for the shaper location within @scope
	 * @new_parent_id: identifier of the new parent for this shaper
	 * @extack: Netlink extended ACK for reporting errors.
	 *
	 * Move the specified shaper in the hierarcy replacing its
	 * current parent shaper with @new_parent_id
	 *
	 * Return:
	 * * %0 - Success
	 * * %-EOPNOTSUPP - Operation is not supported by hardware, driver, or core
	 *                  for any reason. @extack should be set to text describing
	 *                  the reason.
	 * * Other negative error values on failure.
	 */
	int (*move)(struct net_device *dev, u32 handle,
		    u32 u32 new_parent_handle,
		    struct netlink_ext_ack *extack);
};

#define SHAPER_HANDLE_MAJOR_PF	-1

/**
 * shaper_make_handle - creates an unique shaper identifier
 * @scope: the shaper scope
 * @major: virtual function number or SHAPER_HANDLE_MAJOR_PF
 * @minor: queue group or queue id or 0
 *
 * Return: an unique indentifier for the shaper
 *
 * Combines the speficified arguments to create an unique indentifier for
 * the shaper.
 * The @major number is only used with @SHAPER_SCOPE_VF,
 * @SHAPER_SCOPE_QUEUE_GROUP and @SHAPER_SCOPE_QUEUE and must be the
 * virtual function number or SHAPER_HANDLE_MAJOR_PF when referring to
 * queues or queues group under the phisical function.
 * The @minor number is only used for @SHAPER_SCOPE_QUEUE_GROUP and
 * @SHAPER_SCOPE_QUEUE, and must bere spectively the queue group
 * identifier or the queue number.
 */
u32 shaper_make_handle(enum shaper_scope scope, int major, int minor);

/*
 * Examples:
 * - set shaping on a given queue
 *   struct shaper_info info = { // fill this
 *   				};
 *   u32 handle = shaper_make_handle(SHAPER_SCOPE_QUEUE, 0, queue_id);
 *   dev->shaper_ops->add(dev, handle, &info, NULL);
 *
 * - create a queue group with a queue group shaping limits.
 *   Assuming the following topology already exists:
 *			< netdev shaper  >
 *			/		\
 *		<queue 0 shaper> . . .  <queue N shaper>
 *
 *   struct shaper_info ginfo = { // fill this
 *   				};
 *   u32 ghandle = shaper_make_handle(SHAPER_SCOPE_QUEUE_GROUP, 0, 0);
 *   dev->shaper_ops->add(dev, ghandle, &ginfo);
 *
 *   // now topology is:
 *   //			        < netdev shaper  >
 *   // 		            /         |          \
 *   // 		           /          |       < newly created shaper  >
 *   //		                  /           |
 *   //	<queue 0 shaper> . . .    <queue N shaper>
 *
 *   // move a shapers for queues 3..n out of such queue group
 *   for (i = 0; i <= 2; ++i) {
 *       u32 qhandle = shaper_make_handle(SHAPER_SCOPE_QUEUE, 0, i);
 *       dev->shaper_ops->move(dev, qhandle, ghandle, NULL);
 *   }
 *
 *   // now the topology is:
 *   //			         < netdev shaper  >
 *   // 				     /            \
 *   // 		< newly created shaper>   <queue 3 shaper> .. <queue n shaper>
 *   //			/		\
 *   //	<queue 0 shaper> . . .    <queue 2 shaper>
 */
#endif

