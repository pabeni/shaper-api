#include <net/net_shaper.h>

#define SHAPER_HANDLE_MINOR_MASK	GENMASK(15, 0)
#define SHAPER_HANDLE_MAJOR_MASK	GENMASK(28, 16)
#define SHAPER_HANDLE_SCOPE_MASK	GENMASK(31, 29)

u32 shaper_make_handle(enum shaper_scope scope, int major, int minor)
{
	u32 handle = FIELD_PREP(SHAPER_HANDLE_SCOPE_MASK, scope);

	/* PORT and NETDEV are unique per device */
	if (scope == SHAPER_SCOPE_PORT || scope == SHAPER_SCOPE_NETDEV)
		return handle;

	/* VFs handle must additionally include the vf number */
	handle |= FIELD_PREP(SHAPER_HANDLE_MAJOR_MASK, major);
	if (scope == SHAPER_SCOPE_VF)
		return handle;

	/* queues and queues group handle must include also the obj id */
	return handle | FIELD_PREP(SHAPER_HANDLE_MINOR_MASK, minor);
}


